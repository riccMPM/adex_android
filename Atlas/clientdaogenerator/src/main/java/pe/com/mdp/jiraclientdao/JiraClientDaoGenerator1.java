/*
package pe.com.mdp.jiraclientdao;

import de.greenrobot.daogenerator.DaoGenerator;
import de.greenrobot.daogenerator.Entity;
import de.greenrobot.daogenerator.Schema;

public class JiraClientDaoGenerator1 {

    public static void main(String args[]) throws Exception {
        Schema schema = new Schema(1, "pe.com.mdp.jiraclient.model.greenDao");

        */



/*Entidades Internas*//*

 Entity productionRep = schema.addEntity("ProductionReportSum");
        productionRep.addIdProperty().autoincrement();
        productionRep.addStringProperty("assigneeUserKey");
        productionRep.addStringProperty("assigneeUserName");
        productionRep.addStringProperty("assigneeJobPosition");
        productionRep.addIntProperty("timeWorked");

        Entity productionItem = schema.addEntity("ProductionReportItem");
        productionItem.addIdProperty().autoincrement();
        productionItem.addStringProperty("assigneeUserKey");
        productionItem.addLongProperty("issueTypeId");
        productionItem.addStringProperty("issueTypeName");
        productionItem.addIntProperty("timeWorked");

        Entity contactV2 = schema.addEntity("Contact");
        contactV2.addLongProperty("idContact").primaryKey();
        contactV2.addLongProperty("idGroup");
        contactV2.addStringProperty("conJiraKey");
        contactV2.addStringProperty("conJobPosition");
        contactV2.addStringProperty("conNames");
        contactV2.addStringProperty("conSurnames");
        contactV2.addStringProperty("conFatherSurname");
        contactV2.addStringProperty("conMotherSurname");
        contactV2.addStringProperty("conBirthday");
        contactV2.addStringProperty("conStatusText");
        contactV2.addStringProperty("conStatusIcon");
        contactV2.addStringProperty("conLastStatusDate");
        contactV2.addStringProperty("conPhotoUrl");
        contactV2.addStringProperty("conDeviceOS");
        contactV2.addStringProperty("conDeviceToken");

        Entity contactEmail = schema.addEntity("Parameter");
        contactEmail.addLongProperty("idParameter").primaryKey();
        contactEmail.addLongProperty("idTypeParameter");
        contactEmail.addStringProperty("parameterName");
        contactEmail.addStringProperty("parameterValue");


         Entity contactV2 = schema.addEntity("Contact");
        contactV2.addLongProperty("idContact").primaryKey();
        contactV2.addLongProperty("idGroup");
        contactV2.addStringProperty("conJiraKey");
        contactV2.addStringProperty("conJobPosition");
        contactV2.addStringProperty("conNames");
        contactV2.addStringProperty("conSurnames");
        contactV2.addStringProperty("conBirthday");
        contactV2.addStringProperty("conStatusText");
        contactV2.addStringProperty("conStatusIcon");
        contactV2.addStringProperty("conLastStatusDate");
        contactV2.addStringProperty("conPhotoUrl");
        contactV2.addStringProperty("conDeviceOS");
        contactV2.addStringProperty("conDeviceToken");

         Entity issueHistory = schema.addEntity("IssueStatus");
        issueHistory.addLongProperty("idStatus").primaryKey();
        issueHistory.addStringProperty("statusName");
        issueHistory.addBooleanProperty("requiredFields");


        Entity issueActivity = schema.addEntity("IssueActivity");
        issueActivity.addLongProperty("idIssue").primaryKey();
        issueActivity.addLongProperty("idIssueHistory");
        issueActivity.addLongProperty("idStatus");
        issueActivity.addStringProperty("statusName");
        issueActivity.addLongProperty("idProject");
        issueActivity.addStringProperty("issueKey");
        issueActivity.addStringProperty("assigneeUserName");
        issueActivity.addStringProperty("assigneeUserKey");
        issueActivity.addStringProperty("summary");
        issueActivity.addStringProperty("description");
        issueActivity.addIntProperty("timeEstimate");
        issueActivity.addIntProperty("timeSpent");
        issueActivity.addStringProperty("startDate");
        issueActivity.addStringProperty("dueDate");


        Entity issueHistory = schema.addEntity("IssueHistory");
        issueHistory.addLongProperty("idIssue").primaryKey();
        issueHistory.addLongProperty("idSprint");
        issueHistory.addLongProperty("idStatus");
        issueHistory.addStringProperty("statusName");
        issueHistory.addLongProperty("idProject");
        issueHistory.addStringProperty("assigneeUserName");
        issueHistory.addStringProperty("assigneeUserKey");
        issueHistory.addStringProperty("summary");
        issueHistory.addStringProperty("description");
        issueHistory.addIntProperty("timeEstimate");
        issueHistory.addIntProperty("timeSpent");
        issueHistory.addStringProperty("startDate");
        issueHistory.addStringProperty("dueDate");


        Entity sprint = schema.addEntity("Sprint");
        sprint.addLongProperty("idSprint").primaryKey();
        sprint.addLongProperty("idProject");
        sprint.addStringProperty("sprintName");
        sprint.addIntProperty("sprintStatus");

      Entity rdaEmployee = schema.addEntity("Location");
        rdaEmployee.addLongProperty("idLocation").primaryKey();
        rdaEmployee.addStringProperty("locName");
        rdaEmployee.addFloatProperty("locLatitude");
        rdaEmployee.addFloatProperty("locLongitude");
        rdaEmployee.addStringProperty("locAddress");
        rdaEmployee.addLongProperty("idOrganization");
        rdaEmployee.addStringProperty("locOrganizationName");

      Entity rdaEmployee = schema.addEntity("RDAService");
        rdaEmployee.addLongProperty("idRDAService").primaryKey();
        rdaEmployee.addStringProperty("rdaServiceStartDate");
        rdaEmployee.addStringProperty("rdaServiceEndDate");
        rdaEmployee.addStringProperty("rdaServiceEmployeeName");
        rdaEmployee.addStringProperty("rdaServiceEmployeeUser");
        rdaEmployee.addLongProperty("rdaServiceEmployeeRoleId");
        rdaEmployee.addStringProperty("rdaServiceEmployeeRoleName");
        rdaEmployee.addDoubleProperty("rdaServiceClientRate");

      Entity rdaEmployee = schema.addEntity("RDAServiceTypeByRDAType");
        rdaEmployee.addIdProperty().autoincrement();
        rdaEmployee.addLongProperty("idRDAType");
        rdaEmployee.addLongProperty("idRDAServiceType");
        rdaEmployee.addStringProperty("rdaType");
        rdaEmployee.addStringProperty("rdaServiceType");
        rdaEmployee.addIntProperty("rdaPeriodTimeSpent");

   Entity rdaEmployee = schema.addEntity("RDAEmployeeByRDAType");
        rdaEmployee.addIdProperty().autoincrement();
        rdaEmployee.addStringProperty("rdaAssigneeUserKey");
        rdaEmployee.addLongProperty("idRDAType");
        rdaEmployee.addStringProperty("rdaType");
        rdaEmployee.addIntProperty("rdaTimeSpent");


Entity rda = schema.addEntity("RDA");
        rda.addLongProperty("idRDA").primaryKey();
        rda.addLongProperty("idProject");
        rda.addStringProperty("rdaProjectName");
        rda.addStringProperty("rdaAssigneeUserKey");
        rda.addStringProperty("rdaAssigneeUserName");
        rda.addStringProperty("rdaDescription");
        rda.addLongProperty("idRDAType");
        rda.addStringProperty("rdaType");
        rda.addLongProperty("idRDAServiceType");
        rda.addStringProperty("rdaServiceType");
        rda.addStringProperty("rdaServiceName");
        rda.addStringProperty("rdaCode");
        rda.addLongProperty("idRDAProjectAssignee");
        rda.addStringProperty("rdaProjectAssigneeName");
        rda.addIntProperty("rdaTimeSpent");
        rda.addStringProperty("rdaStartDate");
        rda.addStringProperty("rdaWorkDescription");
        rda.addStringProperty("dateFilterStart");
        rda.addStringProperty("dateFilterEnd");
        rda.addStringProperty("arrStrProjectIds");

        Entity rdaPeriod = schema.addEntity("RDAPeriod");
        rdaPeriod.addIdProperty().autoincrement();
        rdaPeriod.addLongProperty("idProject");
        rdaPeriod.addStringProperty("rdaPeriodProjectName");
        rdaPeriod.addStringProperty("rdaPeriodAssigneeUserKey");
        rdaPeriod.addStringProperty("rdaPeriodAssigneeUserName");
        rdaPeriod.addStringProperty("rdaPeriodTimeSpent");
        rdaPeriod.addStringProperty("dateFilterStart");
        rdaPeriod.addStringProperty("dateFilterEnd");
        rdaPeriod.addStringProperty("arrStrProjectIds");

        Entity rdaProjectAssignee = schema.addEntity("RDAProjectAssignee");
        rdaProjectAssignee.addLongProperty("idRDAProjectAssignee").primaryKey();
        rdaProjectAssignee.addStringProperty("rdaProjectAssigneeName");
        rdaProjectAssignee.addLongProperty("idProject");

        Entity rdaServiceType = schema.addEntity("RDAServiceType");
        rdaServiceType.addLongProperty("idRDAServiceType").primaryKey();
        rdaServiceType.addStringProperty("rdaServiceType");
        rdaServiceType.addLongProperty("idProject");

        Entity rdaType = schema.addEntity("RDAType");
        rdaType.addLongProperty("idRDAType").primaryKey();
        rdaType.addStringProperty("rdaType");
        rdaType.addLongProperty("idProject");

        Entity checkIn = schema.addEntity("CheckIn");
        checkIn.addIdProperty();
        checkIn.addStringProperty("userName");
        checkIn.addStringProperty("userDisplayName");
        checkIn.addLongProperty("userId");
        checkIn.addDateProperty("date");
        checkIn.addStringProperty("checkInTime");
        checkIn.addStringProperty("checkOutTime");
        checkIn.addStringProperty("locationCheckInDesc");
        checkIn.addDoubleProperty("locationCheckInLat");
        checkIn.addDoubleProperty("locationCheckInLng");
        checkIn.addStringProperty("locationCheckOutDesc");
        checkIn.addDoubleProperty("locationCheckOutLat");
        checkIn.addDoubleProperty("locationCheckOutLng");
        checkIn.addLongProperty("issueIdCheckIn");
        checkIn.addLongProperty("issueIdCheckOut");

        new DaoGenerator().generateAll(schema, "app/src/main/java");
        //new DaoGenerator().generateAll(schema, "../app/src/main/java");

          Entity contactEmail = schema.addEntity("ContactEmail");
        contactEmail.addLongProperty("idContact");
        contactEmail.addLongProperty("rowId").primaryKey();
        contactEmail.addStringProperty("email");
        contactEmail.addStringProperty("emailType");
        contactEmail.addIntProperty("emailVisibility");

        Entity contactPhone = schema.addEntity("ContactPhone");
        contactPhone.addLongProperty("idContact");
        contactPhone.addLongProperty("rowId").primaryKey();
        contactPhone.addStringProperty("phoneType");
        contactPhone.addStringProperty("phone");
        contactPhone.addStringProperty("phoneAnnex");
        contactPhone.addIntProperty("phoneVisibility");
    }

}
*/
