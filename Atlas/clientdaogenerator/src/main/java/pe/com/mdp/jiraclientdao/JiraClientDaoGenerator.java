package pe.com.mdp.jiraclientdao;

import de.greenrobot.daogenerator.DaoGenerator;
import de.greenrobot.daogenerator.Entity;
import de.greenrobot.daogenerator.Schema;

public class JiraClientDaoGenerator {

    public static void main(String args[]) throws Exception {
        Schema schema = new Schema(1, "pe.com.mdp.appdom.model.greenDao");

        Entity productionRep = schema.addEntity("ServiceReportItem");
        productionRep.addLongProperty("idServiceReport").primaryKey();
        productionRep.addStringProperty("assignedStartDate");
        productionRep.addStringProperty("assignedEndDate");
        productionRep.addStringProperty("assigneeName");
        productionRep.addStringProperty("assigneeJiraUser");
        productionRep.addLongProperty("assigneeJobPositionId");
        productionRep.addStringProperty("assigneeJobPosition");
        productionRep.addDoubleProperty("assigneeClientRate");





        new DaoGenerator().generateAll(schema, "/Users/Riccardomp/Documents/android/Atlas");
    }

}
