/*
package pe.com.mdp.jiraclientdao;

import de.greenrobot.daogenerator.DaoGenerator;
import de.greenrobot.daogenerator.Entity;
import de.greenrobot.daogenerator.Property;
import de.greenrobot.daogenerator.Schema;
import de.greenrobot.daogenerator.ToMany;

public class JiraClientDaoGeneratorBkp {

    public static void main(String args[]) throws Exception {
        Schema schema = new Schema(1, "pe.com.mdp.jiraclient.model");

        */
/*Entidades Maestras*//*

        Entity priority = schema.addEntity("Priority");
        priority.addIdProperty();
        priority.addLongProperty("priorityId"); //valor de id en WS
        priority.addStringProperty("statusColor");
        priority.addStringProperty("description");
        priority.addStringProperty("iconUrl");
        priority.addStringProperty("name");

        Entity statusCategory = schema.addEntity("StatusCategory");
        statusCategory.addIdProperty();
        statusCategory.addLongProperty("statusCategoryId");
        statusCategory.addStringProperty("key");
        statusCategory.addStringProperty("colorName");
        statusCategory.addStringProperty("name");

        Entity status = schema.addEntity("Status");
        status.addIdProperty();
        status.addLongProperty("statusId");
        status.addStringProperty("description");
        status.addStringProperty("iconUrl");
        status.addStringProperty("name");

        Property statusCategoryId = status.addLongProperty("statusCategoryId").getProperty();
        status.addToOne(statusCategory, statusCategoryId, "feStatusCategory");
        ToMany statusCategoryToStatus = statusCategory.addToMany(status, statusCategoryId);
        statusCategoryToStatus.setName("feeStatuses");

        Entity projectCategory = schema.addEntity("ProjectCategory");
        projectCategory.addIdProperty();
        projectCategory.addLongProperty("projectCategoryId");
        projectCategory.addStringProperty("description");
        projectCategory.addStringProperty("name");

        Entity issueType = schema.addEntity("IssueType");
        issueType.addIdProperty();
        issueType.addLongProperty("issueTypeId");
        issueType.addStringProperty("description");
        issueType.addStringProperty("iconUrl");
        issueType.addStringProperty("name");
        issueType.addBooleanProperty("subtask");
        issueType.addLongProperty("avatarId"); //almacenandolo para la fase 2?

        */
/*Entidades de Seguridad*//*

        Entity group = schema.addEntity("Group");
        group.addIdProperty();
        group.addLongProperty("groupId"); //este campo es ficticio, no existe en WS
        group.addStringProperty("name");

        Entity user = schema.addEntity("User");
        user.addIdProperty();
        user.addStringProperty("key");
        user.addStringProperty("name");
        user.addStringProperty("emailAddress");
        user.addStringProperty("avatarUrl"); //el de 48x48
        user.addStringProperty("displayName");
        user.addBooleanProperty("active");
        user.addStringProperty("timeZone");
        user.addStringProperty("locale");

        Property groupId = user.addLongProperty("groupId").getProperty();
        user.addToOne(group, groupId, "feGroup");
        ToMany groupToUser = group.addToMany(user, groupId);
        groupToUser.setName("feeUsers");

        */
/*El proyecto en si no es una entidad de seguridad, pero como se filtra
        la data en base a este se toma como tal*//*

        Entity project = schema.addEntity("Project");
        project.addIdProperty();
        project.addLongProperty("projectId"); //valor de id en WS
        project.addStringProperty("key");
        project.addStringProperty("description");
        project.addStringProperty("name");
        project.addStringProperty("avatarUrl"); //el de 32x32
        project.addLongProperty("categoryId");
        project.addStringProperty("categoryName");
        project.addStringProperty("projectTypeKey");

        Entity permission = schema.addEntity("Permission");
        permission.addIdProperty();
        permission.addLongProperty("permissionId"); //valor de id en WS
        permission.addStringProperty("key");
        permission.addStringProperty("name");
        permission.addStringProperty("type");
        permission.addStringProperty("description");
        permission.addBooleanProperty("havePermission");
        permission.addBooleanProperty("deprecatedKey");

        */
/*Entidades de soporte - Usadas por varias Entidades para almacenar info*//*

        Entity comment = schema.addEntity("Comment");
        comment.addIdProperty();
        comment.addLongProperty("commentId");
        comment.addStringProperty("body");
        comment.addStringProperty("created");
        comment.addStringProperty("updated");

        Property userAuthorId = comment.addLongProperty("userAuthorId").getProperty();
        comment.addToOne(user, userAuthorId, "feUserAuthor");
        ToMany userAuthorToComment = user.addToMany(comment, userAuthorId);
        userAuthorToComment.setName("feeAuthorComments");

        Property userUpdateAuthorId = comment.addLongProperty("userUpdateAuthorId").getProperty();
        comment.addToOne(user, userUpdateAuthorId, "feUserUpdateAuthor");
        ToMany userUpdateAuthorToComment = user.addToMany(comment, userUpdateAuthorId);
        userUpdateAuthorToComment.setName("feeUpdateAuthorComments");

        */
/*Entidades de Negocio*//*

        Entity worklog = schema.addEntity("Worklog");
        worklog.addIdProperty();
        worklog.addLongProperty("worklogId"); //valor de id en WS
        worklog.addStringProperty("issueKey"); //No se puede relacionar logicamente al ser string y no long
        worklog.addStringProperty("comment");
        worklog.addStringProperty("created"); //igual a startDate en GET
        worklog.addStringProperty("updated");
        worklog.addStringProperty("started");
        worklog.addStringProperty("timeSpent");
        worklog.addIntProperty("timeSpentSeconds"); //igual a duration en el GET

        Entity issue = schema.addEntity("Issue");
        issue.addIdProperty();
        issue.addLongProperty("issueId"); //valor de id en WS
        issue.addStringProperty("key");
        issue.addStringProperty("summary");
        issue.addStringProperty("duedate");
        issue.addIntProperty("timespent");
        issue.addIntProperty("timeoriginalestimate");
        issue.addStringProperty("description");

        Property issueTypeId = issue.addLongProperty("issueTypeId").getProperty();
        issue.addToOne(issueType, issueTypeId, "feIssueType");
        ToMany issueTypeToIssue = issueType.addToMany(issue, issueTypeId);
        issueTypeToIssue.setName("feeIssues");

        Property userReporterId = issue.addLongProperty("userReporterId").getProperty();
        issue.addToOne(user, userReporterId, "feUserReporter");
        ToMany userReporterToIssue = user.addToMany(issue, userReporterId);
        userReporterToIssue.setName("feeReportedIssues");

        Property issueId = comment.addLongProperty("issueId").getProperty();
        issue.addToMany(comment, issueId, "feIssueComments");

        Property userAssigneeId = issue.addLongProperty("userAssigneeId").getProperty();
        issue.addToOne(user, userAssigneeId, "feUserAsignee");
        ToMany userAssigneeToIssue = user.addToMany(issue, userAssigneeId);
        userAssigneeToIssue.setName("feeAssignedIssues");

        Property priorityId = issue.addLongProperty("priorityId").getProperty();
        issue.addToOne(priority, priorityId, "fePriority");
        ToMany priorityToIssue = priority.addToMany(issue, priorityId);
        priorityToIssue.setName("feeIssues");

        Property statusId = issue.addLongProperty("statusId").getProperty();
        issue.addToOne(status, statusId, "feStatus");
        ToMany statusToIssue = status.addToMany(issue, statusId);
        statusToIssue.setName("feeIssues");

        new DaoGenerator().generateAll(schema, "../app/src/main/java");
    }

}
*/
