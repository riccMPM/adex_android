package com.greendao;

import org.greenrobot.greendao.generator.DaoGenerator;
import org.greenrobot.greendao.generator.Entity;
import org.greenrobot.greendao.generator.Schema;

public class MyGenerator {
    public static void main(String[] args) {
        Schema schema = new Schema(1, "pe.com.ricc.atlas.db"); // Your app package name and the (.db) is the folder where the DAO files will be generated into.
        schema.enableKeepSectionsByDefault();

        addTables(schema);

        try {
            new DaoGenerator().generateAll(schema,"./app/src/main/java");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static void addTables(final Schema schema) {
        addDefinitiondEntities(schema);
        addHistoryEntitie(schema);
    }

    // This is use to describe the colums of your table
    private static Entity addDefinitiondEntities(final Schema schema) {
        Entity definition = schema.addEntity("Definition");
        definition.addStringProperty("id").primaryKey();
        definition.addIntProperty("idFather");
        definition.addStringProperty("world").notNull();
        definition.addStringProperty("worldASCII").notNull();
        definition.addStringProperty("definition");
        definition.addBooleanProperty("isTerrestral");
        definition.addBooleanProperty("isMaritime");
        definition.addBooleanProperty("isAereal");
        definition.addBooleanProperty("isINCOTERMS");
        definition.addBooleanProperty("isOperadorComercioExterior");
        definition.addBooleanProperty("isAcuerdoComercialPeru");
        definition.addBooleanProperty("isTipoContenedores");
        definition.addBooleanProperty("isTipoCarga");
        definition.addBooleanProperty("isTipoBuqueMercante");
        definition.addBooleanProperty("isREGIMENS");
        definition.addStringProperty("urlImage");
        definition.addStringProperty("urlVideo");
        definition.addBooleanProperty("isHistory");
        definition.addBooleanProperty("isShowLetter");
        definition.addDateProperty("creationDate");
        definition.addStringProperty("source");
        return definition;
    }
    private static Entity addHistoryEntitie(final Schema schema) {
        Entity definition = schema.addEntity("History");
        definition.addIdProperty().autoincrement();
        definition.addLongProperty("idDefinition").notNull();
        definition.addStringProperty("worldDefinition");
        definition.addIntProperty("idImageCategory");
        return definition;
    }
}
