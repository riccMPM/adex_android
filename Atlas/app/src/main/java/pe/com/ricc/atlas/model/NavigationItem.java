package pe.com.ricc.atlas.model;

/**
 * Created by Riccardomp on 30/04/17.
 */

public class NavigationItem {
    private int id;
    private int color;
    private String descripcion;
    private boolean seleccionado;

    public NavigationItem(int id, int color, String descripcion, boolean seleccionado) {
        this.setId(id);
        this.setColor(color);
        this.setDescripcion(descripcion);
        this.setSeleccionado(seleccionado);

    }
    public NavigationItem(int id) {
        this.setId(id);
        this.setColor(0);
        this.setDescripcion("");
        this.setSeleccionado(false);

    }


    public int getColor() {
        return color;
    }

    public void setColor(int color) {
        this.color = color;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public boolean isSeleccionado() {
        return seleccionado;
    }

    public void setSeleccionado(boolean seleccionado) {
        this.seleccionado = seleccionado;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
