package pe.com.ricc.atlas.application;

import android.app.Application;


import org.greenrobot.greendao.database.Database;

import pe.com.ricc.atlas.dao.DaoMaster;
import pe.com.ricc.atlas.dao.DaoSession;
import pe.com.ricc.atlas.util.Constants;

/**
 * Created by Lenovo on 15/04/2016.
 */
public class AtlasApplication extends Application {
    private DaoSession daoSession;


    @Override
    public void onCreate() {
        super.onCreate();

        DaoMaster.DevOpenHelper helper = new DaoMaster.DevOpenHelper(this, Constants.APP_DB_NAME); //The users-db here is the name of our database.
        Database db = helper.getWritableDb();
        daoSession = new DaoMaster(db).newSession();

    }

    public DaoSession getDaoSession() {
        return daoSession;
    }



}
