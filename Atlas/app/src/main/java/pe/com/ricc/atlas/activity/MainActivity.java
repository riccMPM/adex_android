package pe.com.ricc.atlas.activity;


import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TableRow;
import android.widget.TextView;


import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import pe.com.ricc.atlas.R;
import pe.com.ricc.atlas.activity.category.CategoryFragment;
import pe.com.ricc.atlas.activity.principal.PrincipalFragment;
import pe.com.ricc.atlas.activity.recent.RecentFragment;
import pe.com.ricc.atlas.adapter.MenuAdapter;


import pe.com.ricc.atlas.model.NavigationItem;
import pe.com.ricc.atlas.util.Constants;
import pe.com.ricc.atlas.util.EnumTipoCategoria;

/**
 * Created by Riccardomp on 20/04/17.
 */

public class MainActivity extends BaseActivity implements MenuAdapter.AdapterCallback, PrincipalFragment.OnFragmentInteractionListener {

    @BindView(R.id.drawerLayout)
    DrawerLayout drawerLayout;
    @BindView(R.id.nav_view)
    NavigationView navigationView;
    @BindView(R.id.rowHome)
    TableRow rowHome;
    @BindView(R.id.rowRecent)
    TableRow rowRecent;
    @BindView(R.id.rlaNavigation)
    RelativeLayout rlaNavigation;
    @BindView(R.id.rviMenuItems)
    RecyclerView rviMenuItems;
    @BindView(R.id.tviCompany)
    TextView tviCompany;


    private MenuAdapter menuAdapter;
    private RecyclerView.LayoutManager layoutManager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        setToolbar(); // Setear Toolbar como action bar
        tviCompany.setText(String.format(getString(R.string.navigation_company), getString(R.string.app_name)));

        if (navigationView != null) {
            showMenu();
            //setupDrawerContent(navigationView);

            //Add margin top RelativeLayout DrawerContetn
            ViewGroup.MarginLayoutParams params = (ViewGroup.MarginLayoutParams) rlaNavigation.getLayoutParams();
            params.topMargin = getStatusBarHeight();
        }


        if (savedInstanceState == null)
            selectItem(null);

    }
    //region events


    @Override
    protected void onResume() {
        super.onResume();
        generateRandomDefinition();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                drawerLayout.openDrawer(GravityCompat.START);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {

        if (drawerLayout != null && drawerLayout.isDrawerOpen(GravityCompat.START)) {
            super.onBackPressed();
        } else {
            drawerLayout.openDrawer(GravityCompat.START);

        }
    }

    @OnClick(R.id.rowHome)
    public void clickHome(View view) {
        selectItem(new NavigationItem(Constants.NAVIGATION_HOME));
        behaviorHighlightMenu(Constants.BEHAVIOR_HOME);

    }

    @OnClick(R.id.rowRecent)
    public void clickRecent(View view) {
        selectItem(new NavigationItem(Constants.NAVIGATION_RECENT));
        behaviorHighlightMenu(Constants.BEHAVIOR_RECENT);
    }

    /**
     * Interface called from MenuAdapter
     */
    @Override
    public void onMenuCallback(NavigationItem navigationItem) {
        behaviorHighlightMenu(Constants.BEHAVIOR_CATEGORIES);
        menuAdapter.updateItems();
        menuAdapter.highlightItem(navigationItem.getId());
        selectItem(navigationItem);
    }

    /**
     * Interface called from PrincipalFragment, the event was triggered by the event seeAll
     */
    @Override
    public void onIntentRecent() {
        selectItem(new NavigationItem(Constants.NAVIGATION_RECENT));
        behaviorHighlightMenu(Constants.BEHAVIOR_RECENT);
    }

    //endregion

    //region method
    private void setToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        final ActionBar ab = getSupportActionBar();
        if (ab != null) {
            // Poner ícono del drawer toggle
            ab.setHomeAsUpIndicator(R.drawable.ic_menu);
            ab.setDisplayHomeAsUpEnabled(true);
        }

    }

    private void selectItem(NavigationItem navigationItem) {
        int idCategory = Constants.NAVIGATION_HOME;

        if (navigationItem != null) {
            idCategory = navigationItem.getId();
            setTitle(navigationItem.getDescripcion());
        } else
            setTitle("");


        Fragment fragment = getFragment(idCategory);

        if (fragment != null) {
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager
                    .beginTransaction()
                    .replace(R.id.main_content, fragment)
                    .commit();
        }
        drawerLayout.closeDrawers();


    }

    private Fragment getFragment(int idCategory) {

        switch (idCategory) {
            case Constants.NAVIGATION_HOME:
                return new PrincipalFragment();
            case Constants.NAVIGATION_RECENT:
                return new RecentFragment();
            case Constants.NAVIGATION_TERRESTRAL:
                return CategoryFragment.newInstance(idCategory);
            case Constants.NAVIGATION_MARITIME:
                return CategoryFragment.newInstance(idCategory);
            case Constants.NAVIGATION_AEREAL:
                return CategoryFragment.newInstance(idCategory);
            case Constants.NAVIGATION_INCOTERMS:
                return CategoryFragment.newInstance(idCategory);
            case Constants.NAVIGATION_OPERADOR_COMERCIO_EXTERIOR:
                return CategoryFragment.newInstance(idCategory);
            case Constants.NAVIGATION_ACUERDO_COMERCIAL_PERU:
                return CategoryFragment.newInstance(idCategory);
            case Constants.NAVIGATION_TIPO_CONTENEDORES:
                return CategoryFragment.newInstance(idCategory);
            case Constants.NAVIGATION_TIPO_CARGA:
                return CategoryFragment.newInstance(idCategory);
            case Constants.NAVIGATION_TIPO_BUQUE_MERCANTE:
                return CategoryFragment.newInstance(idCategory);
            case Constants.NAVIGATION_REGIMENS:
                return CategoryFragment.newInstance(idCategory);
            case Constants.NAVIGATION_GENERAL:
                return CategoryFragment.newInstance(idCategory);
            default:
                showToast("Default");
                return null;

        }
    }

    public int getStatusBarHeight() {
        int result = 0;
        int resourceId = getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = getResources().getDimensionPixelSize(resourceId);
        }
        return result;
    }

    private void showMenu() {
        List<NavigationItem> lstNavigation = new ArrayList<>();
        for (int i = 0; i < EnumTipoCategoria.values().length; i++) {
            EnumTipoCategoria enumTipoInformacion = EnumTipoCategoria.values()[i];
            lstNavigation.add(new NavigationItem(enumTipoInformacion.getValue(), enumTipoInformacion.getColor(), enumTipoInformacion.getDescripcion(), false));
        }
        menuAdapter = new MenuAdapter(this, lstNavigation, this);
        layoutManager = new LinearLayoutManager(this);

        rviMenuItems.setHasFixedSize(true);
        rviMenuItems.setLayoutManager(layoutManager);
        rviMenuItems.setAdapter(menuAdapter);

        behaviorHighlightMenu(Constants.BEHAVIOR_HOME);

    }


    private void behaviorHighlightMenu(int idBehavior) {
        switch (idBehavior) {
            case Constants.BEHAVIOR_HOME:
                menuAdapter.updateItems();
                rowHome.setBackgroundColor(ResourcesCompat.getColor(getResources(), R.color.navigator_item_selected, null));
                rowRecent.setBackgroundColor(ResourcesCompat.getColor(getResources(), android.R.color.transparent, null));
                break;
            case Constants.BEHAVIOR_RECENT:
                menuAdapter.updateItems();
                rowHome.setBackgroundColor(ResourcesCompat.getColor(getResources(), android.R.color.transparent, null));
                rowRecent.setBackgroundColor(ResourcesCompat.getColor(getResources(), R.color.navigator_item_selected, null));
                break;
            case Constants.BEHAVIOR_CATEGORIES:
                rowHome.setBackgroundColor(ResourcesCompat.getColor(getResources(), android.R.color.transparent, null));
                rowRecent.setBackgroundColor(ResourcesCompat.getColor(getResources(), android.R.color.transparent, null));
                break;


            default:

        }

    }


    //endregion
}
