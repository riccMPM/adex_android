package pe.com.ricc.atlas.adapter;

import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import pe.com.ricc.atlas.R;
import pe.com.ricc.atlas.activity.definition.DefinitionActivity;
import pe.com.ricc.atlas.model.History;


/**
 * Created by Lenovo on 19/07/2016.
 */
public class HistoryAdapter extends RecyclerView.Adapter<HistoryAdapter.ViewHolder> {

    private List<History> historyList;

    public HistoryAdapter(List<History> historyList) {
        this.historyList = historyList;

    }


    @Override
    public HistoryAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.item_history, viewGroup, false);

        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(HistoryAdapter.ViewHolder viewHolder, int position) {
        History history = historyList.get(position);

        viewHolder.tviWorld.setText(history.getWorldDefinition());

    }


    @Override
    public int getItemCount() {
        return historyList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        @BindView(R.id.tviWorld)
        TextView tviWorld;


        public ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);


            view.setClickable(true);
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(v.getContext(), DefinitionActivity.class);
                    v.getContext().startActivity(intent);
                }
            });
        }

        @Override
        public void onClick(View v) {


        }

    }


}
