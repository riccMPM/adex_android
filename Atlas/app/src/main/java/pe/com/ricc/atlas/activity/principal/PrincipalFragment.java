package pe.com.ricc.atlas.activity.principal;


import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;


import org.greenrobot.greendao.query.Query;
import org.greenrobot.greendao.query.QueryBuilder;
import org.greenrobot.greendao.query.WhereCondition;

import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import butterknife.OnClick;
import pe.com.ricc.atlas.R;
import pe.com.ricc.atlas.activity.BaseFragment;
import pe.com.ricc.atlas.activity.definition.DefinitionActivity;
import pe.com.ricc.atlas.activity.search.SearchActivity;
import pe.com.ricc.atlas.dao.DefinitionDao;
import pe.com.ricc.atlas.model.Definition;
import pe.com.ricc.atlas.util.Constants;
import pe.com.ricc.atlas.util.FunctionsUtil;

/**
 * Created by Lenovo on 08/12/2016.
 */

public class PrincipalFragment extends BaseFragment {


    @BindView(R.id.tviWorldDay)
    TextView tviWorldDay;
    @BindView(R.id.tviSeeAll)
    TextView tviSeeAll;
    @BindView(R.id.iviHistory)
    ImageView iviHistory;
    @BindView(R.id.eteSearch)
    EditText eteSearch;
    @BindView(R.id.tviDate)
    TextView tviDate;
    @BindView(R.id.tviRecent)
    TextView tviRecent;
    @BindView(R.id.cviShare)
    CardView cviShare;


    private OnFragmentInteractionListener mListener;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        generateRandomDefinition();

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_principal, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setTitle(String.format(getString(R.string.navigation_company), getString(R.string.app_name)));
        eteSearch.setCursorVisible(false);
        String worldDay = (getDefinitionDay() == null) ? getResources().getString(R.string.principal_no_word_day) : getDefinitionDay().getWorld();
        tviWorldDay.setText(worldDay);
        tviDate.setText(FunctionsUtil.getDateSpanish());
        setTextRecent();

    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            mListener = (OnFragmentInteractionListener) context;
        } catch (ClassCastException e) {
            Log.i(Constants.LOG_APP_TAG, e.getMessage(), e);
            throw new ClassCastException(context.toString()
                    + " must implement OnFragmentInteractionListener in PrincipalFragment. " + e.getMessage());
        }
    }
    //region method


    public void setTextRecent() {
        String textRecent = "";
        DefinitionDao definitionDao = getDaoSession().getDefinitionDao();
        QueryBuilder queryBuilder = definitionDao.queryBuilder();
        queryBuilder.where(DefinitionDao.Properties.IsHistory.eq(true));
        queryBuilder.orderDesc(DefinitionDao.Properties.CreationDate);
        List<Definition> lstDefinition = queryBuilder.list();

        for (Definition definition : lstDefinition) {
            textRecent += definition.getWorld() + ", ";
        }
        if ("".equals(textRecent))
            textRecent = getResources().getString(R.string.principal_recent_no_words);

        tviRecent.setText(FunctionsUtil.removeLastWorld(textRecent.trim()));

    }
    //endregion

    //region events
    @OnClick(R.id.eteSearch)
    public void clickSearch() {
        Intent intent = new Intent(getContext(), SearchActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.tviWorldDay)
    public void clickWorlDay() {
        if (!"".equals(getDateWordDay())) {
            Intent intent = new Intent(getContext(), DefinitionActivity.class);
            intent.putExtra(Constants.INT_DEFINITION_OBJECT, getDefinitionDay());
            startActivity(intent);
        }
    }


    @OnClick(R.id.cviShare)
    public void ClickShareFriends() {
        int applicationNameId = getContext().getApplicationInfo().labelRes;
        final String appPackageName = getContext().getPackageName();
        Intent i = new Intent(Intent.ACTION_SEND);
        i.setType("text/plain");
        i.putExtra(Intent.EXTRA_SUBJECT, getContext().getString(applicationNameId));
        String text = getString(R.string.general_download_app);
        String link = "https://play.google.com/store/apps/details?id=" + appPackageName;
        i.putExtra(Intent.EXTRA_TEXT, text + " " + link);
        startActivity(Intent.createChooser(i, getString(R.string.general_share_app)));
    }

    @OnClick(R.id.tviSeeAll)
    public void clickSeeAll() {
        mListener.onIntentRecent();
    }

    @OnClick(R.id.tviRecent)
    public void clickSeeRecent() {
        mListener.onIntentRecent();
    }


    //endregion

    //region interface
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onIntentRecent();

    }
    //endregion

}
