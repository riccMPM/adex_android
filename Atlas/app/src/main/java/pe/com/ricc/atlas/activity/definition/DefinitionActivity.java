package pe.com.ricc.atlas.activity.definition;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;

import net.opacapp.multilinecollapsingtoolbar.CollapsingToolbarLayout;

import android.support.design.widget.FloatingActionButton;
import android.support.v7.graphics.Palette;

import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;


import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import org.greenrobot.greendao.query.QueryBuilder;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import pe.com.ricc.atlas.R;
import pe.com.ricc.atlas.activity.BaseActivity;
import pe.com.ricc.atlas.adapter.RelationAdapter;
import pe.com.ricc.atlas.dao.DefinitionDao;
import pe.com.ricc.atlas.model.Definition;
import pe.com.ricc.atlas.util.Constants;
import pe.com.ricc.atlas.util.FunctionsUtil;

/**
 * Created by Lenovo on 08/12/2016.
 */

public class DefinitionActivity extends BaseActivity {


    @BindView(R.id.appbar)
    AppBarLayout appBarLayout;
    @BindView(R.id.collapsing_toolbar)
    CollapsingToolbarLayout collapsingToolbar;
    @BindView(R.id.tviDescription)
    TextView tviDescription;
    @BindView(R.id.tviSource)
    TextView tviSource;
    @BindView(R.id.fabYoutbe)
    FloatingActionButton fabYoutbe;
    @BindView(R.id.iviDefinition)
    ImageView iviDefinition;
    @BindView(R.id.cviSource)
    CardView cviSource;
    @BindView(R.id.cviRelation)
    CardView cviRelation;
    @BindView(R.id.rviRelation)
    RecyclerView rviRelation;


    private Menu collapsedMenu;
    private boolean appBarExpanded = true;
    private boolean showIconYoutbe = true;
    private Definition definition;

    protected List<Definition> lstDefinition = new ArrayList<>();
    private RecyclerView.LayoutManager layoutManager;
    private RelationAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_definition);
        ButterKnife.bind(this);

        //Add AdMob
        AdView mAdView = (AdView) findViewById(R.id.aviBanner);
        AdRequest adRequest = new AdRequest.Builder()
                //.addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
                // Check the LogCat to get your test device ID
                //.addTestDevice(getString(R.string.banner_ad_unit_qas))
                .build();

        mAdView.loadAd(adRequest);

        definition = (Definition) getIntent().getSerializableExtra(Constants.INT_DEFINITION_OBJECT);

        final Toolbar toolbar = (Toolbar) findViewById(R.id.anim_toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null)
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        updateUI();


    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        if (collapsedMenu != null
                && (!appBarExpanded || collapsedMenu.size() != 1)
                && !showIconYoutbe) {
            //collapsed
            collapsedMenu.add("video")
                    .setIcon(R.drawable.ic_youtube)
                    .setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
        } else {
            //expanded
        }
        return super.onPrepareOptionsMenu(collapsedMenu);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.general_menu, menu);
        collapsedMenu = menu;
        MenuItem item = menu.findItem(R.id.action_clear);
        item.setVisible(false);//
        item = menu.findItem(R.id.action_search);
        item.setVisible(false);//
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            case R.id.action_share:
                shareContent(definition.getWorld() + ":\n" + definition.getDefinition());
                return true;
        }
        if (item.getTitle() == "video") {
            seeVideo();
        }

        return super.onOptionsItemSelected(item);
    }

    //region method
    public void updateUI() {

        showRelation();
        tviDescription.setText(FunctionsUtil.makeSectionOfTextBold(definition.getDefinition()));

        //Verify if the definition has source
        if (!"".equals(definition.getSource()))
            tviSource.setText(definition.getSource());
        else
            cviSource.setVisibility(View.GONE);

        //Verify if the world have relation
        if (lstDefinition.isEmpty())
            cviRelation.setVisibility(View.GONE);

        collapsingToolbar.setTitle(definition.getWorld());

        Bitmap bitmap = BitmapFactory.decodeResource(getResources(),
                R.drawable.img_comercio_exterior);

        Palette.from(bitmap).generate(new Palette.PaletteAsyncListener() {
            @SuppressWarnings("ResourceType")
            @Override
            public void onGenerated(Palette palette) {
                int vibrantColor = palette.getVibrantColor(R.color.primary_500);
                collapsingToolbar.setContentScrimColor(vibrantColor);
                collapsingToolbar.setStatusBarScrimColor(R.color.black_trans80);
            }
        });

        if (!"".equals(definition.getUrlImage()))
            loadImage();

        appBarLayout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                Log.i(DefinitionActivity.class.getSimpleName(), "onOffsetChanged: verticalOffset: " + verticalOffset);

                //  Vertical offset == 0 indicates appBar is fully expanded.
                if (Math.abs(verticalOffset) > 200) {
                    appBarExpanded = false;
                    showIconYoutbe = ("".equals(definition.getUrlVideo())) ? true : false;
                    invalidateOptionsMenu();
                } else {
                    appBarExpanded = true;
                    showIconYoutbe = true;

                    invalidateOptionsMenu();
                }
            }
        });

        if ("".equals(definition.getUrlVideo())) {
            fabYoutbe.setVisibility(View.INVISIBLE);
        }

        fabYoutbe.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                seeVideo();
            }
        });

    }

    public void showRelation() {
        DefinitionDao definitionDao = getDaoSession().getDefinitionDao();
        QueryBuilder qb = definitionDao.queryBuilder();
        qb.where(DefinitionDao.Properties.IdFather.eq(definition.getId()));
        qb.orderAsc(DefinitionDao.Properties.World);
        lstDefinition = qb.list();

        adapter = new RelationAdapter(lstDefinition);
        layoutManager = new LinearLayoutManager(this);

        rviRelation.setHasFixedSize(true);
        rviRelation.setLayoutManager(layoutManager);
        rviRelation.setAdapter(adapter);

    }

    private void seeVideo() {
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(Uri.parse(definition.getUrlVideo()));
        startActivity(intent);
    }

    public void shareContent(Object object) {
        Intent share = new Intent(Intent.ACTION_SEND);
        share.setType("text/plain");
        share.putExtra(Intent.EXTRA_TEXT, (String) object);
        startActivity(Intent.createChooser(share, "Compartir con"));
    }

    void loadImage() {


        Target mTarget = new Target() {
            @Override
            public void onBitmapLoaded(final Bitmap bitmap, Picasso.LoadedFrom from) {


                iviDefinition.setImageBitmap(bitmap);
                Palette.from(bitmap).generate(new Palette.PaletteAsyncListener() {
                    @SuppressWarnings("ResourceType")
                    @Override
                    public void onGenerated(Palette palette) {
                        int vibrantColor = palette.getVibrantColor(R.color.primary_500);
                        collapsingToolbar.setContentScrimColor(vibrantColor);
                        collapsingToolbar.setStatusBarScrimColor(R.color.black_trans80);


                    }
                });
            }

            @Override
            public void onBitmapFailed(Drawable errorDrawable) {
                Log.d(Constants.LOG_APP_TAG, "onBitmapFailed");
            }

            @Override
            public void onPrepareLoad(Drawable placeHolderDrawable) {
                Log.d(Constants.LOG_APP_TAG, "onPrepareLoad");
            }
        };

        Picasso.with(this.getApplicationContext())
                .load(definition.getUrlImage())
                .into(mTarget);
    }
    //endregion
}