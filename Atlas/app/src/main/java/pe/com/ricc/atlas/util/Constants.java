package pe.com.ricc.atlas.util;


/**
 * Created by Lenovo on 20/04/2016.
 */
public class Constants {
    public static final String LOG_APP_TAG = "ATLAS_LOG";
    public static final String APP_DB_NAME = "atlas-db";
    public static final String JSON_DEFINIITION = "dictionaryData.json";

    public static final int TEXT_LENGHT_DEFAULT = 0;
    public static final String MSSQLSRV_DATEFORMAT_105 = "dd-MM-yyyy";

    /* Preferences */
    public static final String PREFERENCES_NAME = "atlasApp";
    public static final String PREF_DATE_WORD_DAY = "dateWordDay";
    public static final String PREF_SYNC_DEFINITION_FIRST_TIME = "synDefinitionFirstTime3";
    public static final String PREF_DEFINITION_DAY = "definitionday";



    /* Intent */
    public static final String INT_DEFINITION_OBJECT = "idDefiniition";

    /* Navigation ID*/
    public static final int NAVIGATION_GENERAL = 0;
    public static final int NAVIGATION_MARITIME = 1;
    public static final int NAVIGATION_TERRESTRAL = 2;
    public static final int NAVIGATION_AEREAL = 3;
    public static final int NAVIGATION_INCOTERMS = 4;
    public static final int NAVIGATION_REGIMENS = 5;
    public static final int NAVIGATION_ACUERDO_COMERCIAL_PERU = 6;
    public static final int NAVIGATION_OPERADOR_COMERCIO_EXTERIOR = 7;
    public static final int NAVIGATION_TIPO_BUQUE_MERCANTE = 8;
    public static final int NAVIGATION_TIPO_CARGA = 9;
    public static final int NAVIGATION_TIPO_CONTENEDORES = 10;
    public static final int NAVIGATION_HOME = 11;
    public static final int NAVIGATION_RECENT = 12;

    /* Behavior ID*/
    public static final int BEHAVIOR_HOME = 1;
    public static final int BEHAVIOR_RECENT = 2;
    public static final int BEHAVIOR_CATEGORIES = 3;

    /* Bundle*/
    public static final String BUNDLE_NAVIGATION = "bundleNavigation";
}

