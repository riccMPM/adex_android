package pe.com.ricc.atlas.activity.recent;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;

import org.greenrobot.greendao.query.QueryBuilder;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import pe.com.ricc.atlas.R;
import pe.com.ricc.atlas.activity.BaseFragment;
import pe.com.ricc.atlas.activity.search.SearchActivity;
import pe.com.ricc.atlas.adapter.RecentAdapter;
import pe.com.ricc.atlas.dao.DefinitionDao;
import pe.com.ricc.atlas.model.Definition;

/**
 * Created by Riccardomp on 1/05/17.
 */

public class RecentFragment extends BaseFragment implements RecentAdapter.AdapterCallback {


    @BindView(R.id.rviRecent)
    RecyclerView rviRecent;


    private RecyclerView.LayoutManager layoutManager;
    private RecentAdapter adapter;
    protected List<Definition> lstDefinition = new ArrayList<>();


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_recent, container, false);
        setHasOptionsMenu(true);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setTitle(getString(R.string.title_recent));

    }

    @Override
    public void onResume() {
        super.onResume();
        showRecent();

    }

    //region event
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.general_menu, menu);
        MenuItem item = menu.findItem(R.id.action_share);
        item.setVisible(false);//
        if (lstDefinition.isEmpty()) {
            item = menu.findItem(R.id.action_clear);
            item.setVisible(false);//
        }

        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_clear:
                clickClear();
                break;
            case R.id.action_search:
                clickSearch();
                break;
        }
        return true;
    }


    //endregion

    //region method
    public void clickClear() {
        new MaterialDialog.Builder(getActivity())
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        clearRecent();
                    }
                })
                .title(getString(R.string.recent_confirm_delete))
                .content(getString(R.string.recent_confirm_description))
                .positiveText(getString(R.string.general_ok))
                .negativeText(getString(R.string.general_cancel))
                .show();
        getActivity().invalidateOptionsMenu();

    }

    private void clearRecent() {
        for (Definition definition : lstDefinition) {
            definition.setIsHistory(false);
        }

        DefinitionDao definitionDao = getDaoSession().getDefinitionDao();
        definitionDao.insertOrReplaceInTx(lstDefinition);
        lstDefinition.clear();
        adapter.clear();
    }

    public void clickSearch() {
        Intent intent = new Intent(getContext(), SearchActivity.class);
        startActivity(intent);
    }

    public void showRecent() {
        generateListRecent();
        adapter = new RecentAdapter(lstDefinition, this);
        layoutManager = new LinearLayoutManager(getContext());

        rviRecent.setHasFixedSize(true);
        rviRecent.setLayoutManager(layoutManager);
        rviRecent.setAdapter(adapter);
        //To verify if exist element in the list
        getActivity().invalidateOptionsMenu();

    }


    private void generateListRecent() {

        DefinitionDao definitionDao = getDaoSession().getDefinitionDao();
        QueryBuilder queryBuilder = definitionDao.queryBuilder();
        queryBuilder.where(DefinitionDao.Properties.IsHistory.eq(true));
        queryBuilder.orderDesc(DefinitionDao.Properties.CreationDate);
        lstDefinition = queryBuilder.list();

    }


    @Override
    public void onInsertHistoryCallback(Definition definition) {


    }
    //endregion


}