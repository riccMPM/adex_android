package pe.com.ricc.atlas.adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.simplecityapps.recyclerview_fastscroll.views.FastScrollRecyclerView;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import pe.com.ricc.atlas.R;
import pe.com.ricc.atlas.activity.definition.DefinitionActivity;
import pe.com.ricc.atlas.model.Definition;
import pe.com.ricc.atlas.util.Constants;
import pe.com.ricc.atlas.util.FunctionsUtil;


/**
 * Created by Lenovo on 19/07/2016.
 */
public class CategoryAdapter extends RecyclerView.Adapter<CategoryAdapter.ViewHolder> implements FastScrollRecyclerView.SectionedAdapter {

    private List<Definition> lstDefinition;
    private AdapterCallback mAdapterCallback;
    private Context context;
    private int idCategory;
    private int lenghtBold = 0;

    public CategoryAdapter(List<Definition> definitionList, int idCategory, AdapterCallback mAdapterCallback, Context context) {
        this.lstDefinition = definitionList;
        this.mAdapterCallback = mAdapterCallback;
        this.context = context;
        this.idCategory = idCategory;
    }

    @Override
    public CategoryAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.item_search_category, viewGroup, false);

        return new ViewHolder(v, mAdapterCallback);
    }

    @Override
    public void onBindViewHolder(CategoryAdapter.ViewHolder viewHolder, int position) {
        Definition definition = lstDefinition.get(position);

        viewHolder.tviWorld.setText(FunctionsUtil.getBoldWorld(definition.getWorld(), getLenghtBold()));
        viewHolder.definition = definition;

        //update image
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
            viewHolder.iviCategory.setImageDrawable(context.getResources().getDrawable(FunctionsUtil.getImageById(idCategory), context.getTheme()));
        else
            viewHolder.iviCategory.setImageDrawable(context.getResources().getDrawable(FunctionsUtil.getImageById(idCategory)));

        if (definition.getIsShowLetter()) {
            int showSeparator = (position > 0) ? View.VISIBLE : View.INVISIBLE;

            viewHolder.tviLetter.setVisibility(View.VISIBLE);
            viewHolder.vSeparator.setVisibility(showSeparator);
            viewHolder.tviLetter.setText(FunctionsUtil.getFirstLetterWorld(definition.getWorld()).toUpperCase());

        } else {
            viewHolder.tviLetter.setVisibility(View.INVISIBLE);
            viewHolder.vSeparator.setVisibility(View.INVISIBLE);
        }
    }


    public void clear() {
        if (lstDefinition == null)
            lstDefinition = new ArrayList<>();

        lstDefinition.clear();
        notifyDataSetChanged();
    }

    // Add a list of items
    public void addAll(List<Definition> list) {
        lstDefinition = new ArrayList<>();
        lstDefinition.addAll(list);
        notifyDataSetChanged();
    }

    public int getLenghtBold() {
        return lenghtBold;
    }

    public void setLenghtBold(int lenghtBold) {
        this.lenghtBold = lenghtBold;
    }

    @NonNull
    @Override
    public String getSectionName(int position) {
        return FunctionsUtil.getFirstLetterWorld(lstDefinition.get(position).getWorld()).toUpperCase();
    }

    @Override
    public int getItemCount() {
        return lstDefinition.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        @BindView(R.id.tviWorld)
        TextView tviWorld;
        @BindView(R.id.tviLetter)
        TextView tviLetter;
        @BindView(R.id.iviCategory)
        ImageView iviCategory;
        @BindView(R.id.vSeparator)
        View vSeparator;

        Definition definition;
        private AdapterCallback mAdapterCallback;

        public ViewHolder(View view, AdapterCallback mAdapterCallback) {
            super(view);
            this.mAdapterCallback = mAdapterCallback;
            ButterKnife.bind(this, view);

            view.setClickable(true);
            view.setOnClickListener(this);

        }

        @Override
        public void onClick(View v) {
            definition.setIsHistory(true);
            long time = System.currentTimeMillis();
            definition.setCreationDate(new Date(time));
            mAdapterCallback.onInsertHistoryCallback(definition);

            Intent intent = new Intent(v.getContext(), DefinitionActivity.class);
            intent.putExtra(Constants.INT_DEFINITION_OBJECT, definition);
            v.getContext().startActivity(intent);

        }

    }

    public interface AdapterCallback {
        void onInsertHistoryCallback(Definition definition);
    }
}
