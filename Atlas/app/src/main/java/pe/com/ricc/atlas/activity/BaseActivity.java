package pe.com.ricc.atlas.activity;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.greenrobot.greendao.query.Query;
import org.greenrobot.greendao.query.QueryBuilder;

import java.lang.reflect.Type;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import pe.com.ricc.atlas.application.AtlasApplication;
import pe.com.ricc.atlas.dao.DaoSession;
import pe.com.ricc.atlas.dao.DefinitionDao;
import pe.com.ricc.atlas.model.Definition;
import pe.com.ricc.atlas.util.Constants;
import pe.com.ricc.atlas.util.EnumTipoCategoria;
import pe.com.ricc.atlas.util.FunctionsUtil;

/**
 * Created by Riccardomp on 20/04/17.
 */

public class BaseActivity extends AppCompatActivity {

    private DaoSession daoSession;
    private SharedPreferences appPreferences;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        daoSession = ((AtlasApplication) getApplicationContext()).getDaoSession();
        appPreferences = getSharedPreferences(Constants.PREFERENCES_NAME, MODE_PRIVATE);
    }

    //region method
    public DaoSession getDaoSession() {
        return daoSession;
    }


    public void hideKeyBoard() {
        View view = getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    public void showToast(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    public void generateDefinitionManual() {

        DefinitionDao definitionDao = getDaoSession().getDefinitionDao();
        definitionDao.deleteAll();


        String jsonStr = FunctionsUtil.loadJSONFromAsset(this, Constants.JSON_DEFINIITION);
        Gson gson = new Gson();
        Type listType = new TypeToken<List<Definition>>() {
        }.getType();
        List<Definition> lstDefinition = gson.fromJson(jsonStr, listType);

        //Sort List
        Collections.sort(lstDefinition, new Comparator<Definition>() {
            public int compare(Definition obj1, Definition obj2) {
                return obj1.getWorld().compareToIgnoreCase(obj2.getWorld()); // To compare string values
            }
        });

        for (Definition definition : lstDefinition) {
            definition.setWorld(definition.getWorld().trim());
            definition.setWorldASCII(FunctionsUtil.getWorldToASCII(definition.getWorld()));
        }

        definitionDao.insertInTx(lstDefinition);
        //updateShowLetterbyCategory();


    }

    public void updateShowLetterbyCategory() {
        //Verify which word is the first to set the field isShowletter

        DefinitionDao definitionDao = getDaoSession().getDefinitionDao();

        for (int i = 0; i < EnumTipoCategoria.values().length; i++) {
            EnumTipoCategoria enumTipoInformacion = EnumTipoCategoria.values()[i];
            int idCategory = enumTipoInformacion.getValue();

            QueryBuilder queryBuilder = definitionDao.queryBuilder();
            //Get condition by Category
            FunctionsUtil.getCategoryConditionWhere(queryBuilder,idCategory);



            List<Definition> lstDefinition = queryBuilder.list();
            for (char alphabet = 'a'; alphabet <= 'z'; alphabet++) {
                for (Definition definition : lstDefinition) {
                    String firstLetter = FunctionsUtil.getFirstLetterWorld(definition.getWorld());

                    if (firstLetter.toLowerCase().equals(String.valueOf(alphabet).toLowerCase())) {
                        definition.setIsShowLetter(true);
                        break;
                    }
                }
            }
            definitionDao.insertOrReplaceInTx(lstDefinition);
        }


    }

    public void generateRandomDefinition() {

        boolean isGetNewWorldDay = (getDateWordDay().equals(FunctionsUtil.getCurrentDate()) ?
                false : true);
        if (isGetNewWorldDay) {
            DefinitionDao definitionDao = getDaoSession().getDefinitionDao();
            Query<Definition> query = definitionDao.queryRawCreate(
                    "ORDER BY RANDOM() LIMIT 1");

            setDateWorldDay(FunctionsUtil.getCurrentDate());

            try {
                setDefinitionDay(query.unique());
            } catch (Exception e) {
                Log.i(Constants.LOG_APP_TAG, e.getMessage());
            }

        }

    }


    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            View v = getCurrentFocus();
            if (v instanceof EditText) {
                Rect outRect = new Rect();
                v.getGlobalVisibleRect(outRect);
                if (!outRect.contains((int) event.getRawX(), (int) event.getRawY())) {
                    v.clearFocus();
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                }
            }
        }
        return super.dispatchTouchEvent(event);
    }

    //endregion

    //region preference

    public void setDefinitionDay(Definition definition) {
        appPreferences.edit().putString(Constants.PREF_DEFINITION_DAY, new Gson().toJson(definition)).apply();
    }

    public Definition getDefinitionDay() {
        return new Gson().fromJson(appPreferences.getString(Constants.PREF_DEFINITION_DAY, ""), Definition.class);
    }

    /*
    * Set the date world of the day
    * */
    public void setDateWorldDay(String date) {
        appPreferences.edit().putString(Constants.PREF_DATE_WORD_DAY, date).apply();
    }

    public String getDateWordDay() {
        if (appPreferences.contains(Constants.PREF_DATE_WORD_DAY))
            return appPreferences.getString(Constants.PREF_DATE_WORD_DAY, "");
        else
            return "";
    }

    public boolean getIsSyncFirstTime() {
        if (appPreferences.contains(Constants.PREF_SYNC_DEFINITION_FIRST_TIME))
            return appPreferences.getBoolean(Constants.PREF_SYNC_DEFINITION_FIRST_TIME, true);
        else
            return true;
    }

    public void setIsSyncFirstTime(boolean value) {
        appPreferences.edit().putBoolean(Constants.PREF_SYNC_DEFINITION_FIRST_TIME, value).apply();
    }
    //endregion
}
