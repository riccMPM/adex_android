package pe.com.ricc.atlas.util;

import pe.com.ricc.atlas.R;

/**
 * Created by Riccardomp on 30/04/17.
 */

public enum EnumTipoCategoria {
    GENERAL(Constants.NAVIGATION_GENERAL, "General", R.color.colorPrimaryText),
    MARITIMO(Constants.NAVIGATION_MARITIME, "Marítimo", R.color.colorPrimaryText),
    TERRESTRE(Constants.NAVIGATION_TERRESTRAL, "Terrestre", R.color.colorPrimaryText),
    AEREO(Constants.NAVIGATION_AEREAL, "Aéreo", R.color.colorPrimaryText),
    INCOTERMS(Constants.NAVIGATION_INCOTERMS, "INCOTERMS ® 2010", R.color.colorPrimaryText),
    REGIMENS(Constants.NAVIGATION_REGIMENS, "Regímenes", R.color.colorPrimaryText),
    ACUERDO_COMERCIAL_PERU(Constants.NAVIGATION_ACUERDO_COMERCIAL_PERU, "Acuerdos Comerciales", R.color.colorPrimaryText),
    OPERADOR_COMERCIO_EXTERIOR(Constants.NAVIGATION_OPERADOR_COMERCIO_EXTERIOR, "Operadores del comercio exterior", R.color.colorPrimaryText),
    TIPO_BUQUE_MERCANTE(Constants.NAVIGATION_TIPO_BUQUE_MERCANTE, "Tipos de Buques Mercantes", R.color.colorPrimaryText),
    TIPO_CARGA(Constants.NAVIGATION_TIPO_CARGA, "Tipos de Carga", R.color.colorPrimaryText),
    TIPO_CONTENEDORES(Constants.NAVIGATION_TIPO_CONTENEDORES, "Tipos de Contenedores", R.color.colorPrimaryText);




    private int value;
    private String descripcion;
    private int color;

    EnumTipoCategoria(int value, String descripcion, int color) {
        this.value = value;
        this.descripcion = descripcion;
        this.color = color;
    }

    public int getValue() {
        return value;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public int getColor() {
        return color;
    }
}
