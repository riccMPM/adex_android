package pe.com.ricc.atlas.adapter;

import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import pe.com.ricc.atlas.R;
import pe.com.ricc.atlas.activity.definition.DefinitionActivity;
import pe.com.ricc.atlas.model.Definition;
import pe.com.ricc.atlas.model.History;
import pe.com.ricc.atlas.util.Constants;


/**
 * Created by Lenovo on 19/07/2016.
 */
public class RelationAdapter extends RecyclerView.Adapter<RelationAdapter.ViewHolder> {

    private List<Definition> definitionList;

    public RelationAdapter(List<Definition> definitionList) {
        this.definitionList = definitionList;

    }


    @Override
    public RelationAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.item_relation, viewGroup, false);

        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(RelationAdapter.ViewHolder viewHolder, int position) {
        Definition definition = definitionList.get(position);

        viewHolder.tviWorld.setText(definition.getWorld());
        viewHolder.definition = definition;

    }


    @Override
    public int getItemCount() {
        return definitionList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        @BindView(R.id.tviWorld)
        TextView tviWorld;
        Definition definition;


        public ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);


            view.setClickable(true);
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(v.getContext(), DefinitionActivity.class);
                    intent.putExtra(Constants.INT_DEFINITION_OBJECT, definition);
                    v.getContext().startActivity(intent);
                }
            });
        }

        @Override
        public void onClick(View v) {


        }

    }


}
