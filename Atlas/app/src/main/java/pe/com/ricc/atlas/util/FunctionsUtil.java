package pe.com.ricc.atlas.util;


import android.content.Context;
import android.graphics.Typeface;
import android.text.SpannableStringBuilder;
import android.text.style.StyleSpan;
import android.util.Log;

import org.greenrobot.greendao.query.QueryBuilder;

import java.io.IOException;
import java.io.InputStream;
import java.text.Normalizer;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


import pe.com.ricc.atlas.R;
import pe.com.ricc.atlas.dao.DefinitionDao;


public class FunctionsUtil {

    private static boolean DISPLAY_DEBUG = true;


    /**
     * Loads content of file from assets as string.
     */
    public static String loadJSONFromAsset(Context context, String name) {
        String json = null;
        try {
            InputStream is = context.getAssets().open(name);
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }

    /**
     * Get the first letter from world.
     */
    public static String getFirstLetterWorld(String world) {
        return String.valueOf(world.charAt(0));
    }

    /**
     * Get the icon of the category
     */
    public static int getImageById(int id) {
        if (Constants.NAVIGATION_MARITIME == id) {
            return R.drawable.ic_boat;
        } else if (Constants.NAVIGATION_TERRESTRAL == id) {
            return R.drawable.ic_bus;
        } else if (Constants.NAVIGATION_AEREAL == id) {
            return R.drawable.ic_flight;
        } else if (Constants.NAVIGATION_INCOTERMS == id) {
            return R.drawable.ic_incoterm;
        } else if (Constants.NAVIGATION_TIPO_BUQUE_MERCANTE == id) {
            return R.drawable.ic_boat;
        } else if (Constants.NAVIGATION_TIPO_CONTENEDORES == id) {
            return R.drawable.ic_contenedor;
        } else if (Constants.NAVIGATION_TIPO_CARGA == id) {
            return R.drawable.ic_box;
        } else if (Constants.NAVIGATION_ACUERDO_COMERCIAL_PERU == id) {
            return R.drawable.ic_world;
        } else if (Constants.NAVIGATION_OPERADOR_COMERCIO_EXTERIOR == id) {
            return R.drawable.ic_world;
        } else {
            return R.drawable.ic_bus;
        }
    }

    /**
     * Get the icon of the category
     */
    public static SpannableStringBuilder getBoldWorld(String world, int lenghtBold) {
        SpannableStringBuilder sb = new SpannableStringBuilder(world);
        lenghtBold = (lenghtBold > world.length()) ? world.length() : lenghtBold;
        sb.setSpan(new StyleSpan(Typeface.BOLD), 0, lenghtBold, 0);
        return sb;
    }

    /**
     * Put bold in some section wit asterisk
     */
    public static SpannableStringBuilder makeSectionOfTextBold(String text) {
        List<String> lstTextToBold = new ArrayList<>();
        Pattern pattern = Pattern.compile("\\*([^\\*]*)\\*");
        Matcher matcher = pattern.matcher(text);
        while (matcher.find()) {
            String word = matcher.group(1).replace("*", "");
            lstTextToBold.add(word);

        }

        String wordWithoutAsterisk = text.replace("*", "");
        SpannableStringBuilder builder = new SpannableStringBuilder(wordWithoutAsterisk);

        for (String textItem : lstTextToBold) {
            if (textItem.length() > 0 && !textItem.trim().equals("")) {
                //for counting start/end indexes
                String testText = wordWithoutAsterisk.toLowerCase();
                String testTextToBold = textItem.toLowerCase();
                int startingIndex = testText.indexOf(testTextToBold);
                int endingIndex = startingIndex + testTextToBold.length();

                if (startingIndex >= 0 && endingIndex >= 0) {
                    builder.setSpan(new StyleSpan(Typeface.BOLD), startingIndex, endingIndex, 0);
                }
            }
        }

        return builder;
    }


    public static String capitalizeFirstLetter(String world) {
        return world.substring(0, 1).toUpperCase() + world.substring(1);
    }

    public static String getDateSpanish() {
        SimpleDateFormat formateador = new SimpleDateFormat("MMMM dd',' yyyy", new Locale("es", "ES"));
        Date fechaDate = new Date();

        return capitalizeFirstLetter(formateador.format(fechaDate));
    }

    /**
     * Convert accent to normal world
     */
    public static String getWorldToASCII(String world) {
        String sd = Normalizer.normalize(world, Normalizer.Form.NFD);
        return sd.replaceAll("[^\\p{ASCII}]", "");
    }

    /**
     * Remove last comma
     */
    public static String removeLastWorld(String str) {
        if (str != null && str.length() > 0 && str.charAt(str.length() - 1) == ',') {
            str = str.substring(0, str.length() - 1);
        }
        return str;
    }


    public static String getCurrentDate() {
        //this method return the current date in this way: 30-11-2016
        String dateFormat = Constants.MSSQLSRV_DATEFORMAT_105;
        SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);

        Calendar currentDate = Calendar.getInstance();
        currentDate.set(Calendar.DAY_OF_MONTH, currentDate.get(Calendar.DAY_OF_MONTH));
        currentDate.set(Calendar.MONTH, currentDate.get(Calendar.MONTH));
        currentDate.set(Calendar.YEAR, currentDate.get(Calendar.YEAR));

        Log.i(Constants.LOG_APP_TAG, "Time: " + sdf.format(currentDate.getTime()));
        return sdf.format(currentDate.getTime());
    }

    public static void getCategoryConditionWhere(QueryBuilder queryBuilder, int idCategory) {
        if (idCategory == Constants.NAVIGATION_AEREAL)
            queryBuilder.where(DefinitionDao.Properties.IsAereal.eq(true));
        else if (idCategory == Constants.NAVIGATION_TERRESTRAL)
            queryBuilder.where(DefinitionDao.Properties.IsTerrestral.eq(true));
        else if (idCategory == Constants.NAVIGATION_MARITIME)
            queryBuilder.where(DefinitionDao.Properties.IsMaritime.eq(true));
        else if (idCategory == Constants.NAVIGATION_ACUERDO_COMERCIAL_PERU)
            queryBuilder.where(DefinitionDao.Properties.IsAcuerdoComercialPeru.eq(true));
        else if (idCategory == Constants.NAVIGATION_INCOTERMS)
            queryBuilder.where(DefinitionDao.Properties.IsINCOTERMS.eq(true));
        else if (idCategory == Constants.NAVIGATION_OPERADOR_COMERCIO_EXTERIOR)
            queryBuilder.where(DefinitionDao.Properties.IsOperadorComercioExterior.eq(true));
        else if (idCategory == Constants.NAVIGATION_REGIMENS)
            queryBuilder.where(DefinitionDao.Properties.IsREGIMENS.eq(true));
        else if (idCategory == Constants.NAVIGATION_TIPO_BUQUE_MERCANTE)
            queryBuilder.where(DefinitionDao.Properties.IsTipoBuqueMercante.eq(true));
        else if (idCategory == Constants.NAVIGATION_TIPO_CARGA)
            queryBuilder.where(DefinitionDao.Properties.IsTipoCarga.eq(true));
        else if (idCategory == Constants.NAVIGATION_TIPO_CONTENEDORES)
            queryBuilder.where(DefinitionDao.Properties.IsTipoContenedores.eq(true));

    }


}
