package pe.com.ricc.atlas.adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import pe.com.ricc.atlas.R;


import pe.com.ricc.atlas.model.NavigationItem;
import pe.com.ricc.atlas.util.Constants;
import pe.com.ricc.atlas.util.FunctionsUtil;


/**
 * Created by Lenovo on 19/07/2016.
 */
public class MenuAdapter extends RecyclerView.Adapter<MenuAdapter.ViewHolder> {

    private List<NavigationItem> lstNavigation;
    private AdapterCallback mAdapterCallback;
    private Context context;

    public MenuAdapter(Context context, List<NavigationItem> lstNavigation, AdapterCallback mAdapterCallback) {
        this.lstNavigation = lstNavigation;
        this.mAdapterCallback = mAdapterCallback;
        this.context = context;
    }


    @Override
    public MenuAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.item_category, viewGroup, false);

        return new ViewHolder(v, mAdapterCallback);
    }

    @Override
    public void onBindViewHolder(MenuAdapter.ViewHolder viewHolder, int position) {
        NavigationItem navigation = lstNavigation.get(position);

        viewHolder.navigationItem = navigation;
        viewHolder.tviMenu.setText(navigation.getDescripcion());

        viewHolder.iviMenu.setColorFilter(ResourcesCompat.getColor(context.getResources(), navigation.getColor(), null),
                android.graphics.PorterDuff.Mode.MULTIPLY);

        //update image
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
            viewHolder.iviMenu.setImageDrawable(context.getResources().getDrawable(FunctionsUtil.getImageById(navigation.getId()), context.getTheme()));
        else
            viewHolder.iviMenu.setImageDrawable(context.getResources().getDrawable(FunctionsUtil.getImageById(navigation.getId())));

        viewHolder.iviMenu.setColorFilter(ResourcesCompat.getColor(context.getResources(), R.color.colorAccent, null));

        if (navigation.isSeleccionado()) {
            viewHolder.rlaMenuAdapter.setBackgroundColor(ResourcesCompat.getColor(context.getResources(), R.color.navigator_item_selected, null));
            viewHolder.tviMenu.setTextColor(ResourcesCompat.getColor(context.getResources(), navigation.getColor(), null));
        } else {
            viewHolder.rlaMenuAdapter.setBackgroundColor(ResourcesCompat.getColor(context.getResources(), android.R.color.transparent, null));
            viewHolder.tviMenu.setTextColor(ResourcesCompat.getColor(context.getResources(), R.color.colorPrimaryText, null));
        }
    }


    public void updateItems() {
        for (NavigationItem navigationItem : lstNavigation) {
            navigationItem.setSeleccionado(false);
        }
        notifyDataSetChanged();
    }

    public void highlightItem(int idNavigation) {
        for (NavigationItem navigationItem : lstNavigation) {
            if (idNavigation == navigationItem.getId()) {
                navigationItem.setSeleccionado(true);
                break;
            }
        }
        notifyDataSetChanged();
    }


    @Override
    public int getItemCount() {
        return lstNavigation.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        @BindView(R.id.tviMenu)
        TextView tviMenu;
        @BindView(R.id.iviMenu)
        ImageView iviMenu;
        @BindView(R.id.rlaMenuAdapter)
        RelativeLayout rlaMenuAdapter;

        private NavigationItem navigationItem;
        private AdapterCallback mAdapterCallback;

        public ViewHolder(View view, AdapterCallback mAdapterCallback) {
            super(view);
            this.mAdapterCallback = mAdapterCallback;
            ButterKnife.bind(this, view);

            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            mAdapterCallback.onMenuCallback(navigationItem);

        }

    }

    public interface AdapterCallback {
        void onMenuCallback(NavigationItem navigationItem);
    }


}
