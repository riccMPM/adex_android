package pe.com.ricc.atlas.activity.search;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;


import org.greenrobot.greendao.query.QueryBuilder;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import pe.com.ricc.atlas.R;
import pe.com.ricc.atlas.activity.BaseActivity;
import pe.com.ricc.atlas.adapter.SearchAdapter;
import pe.com.ricc.atlas.dao.DefinitionDao;
import pe.com.ricc.atlas.model.Definition;
import pe.com.ricc.atlas.util.Constants;
import pe.com.ricc.atlas.util.SimpleDividerItemDecoration;

/**
 * Created by Lenovo on 08/12/2016.
 */

public class SearchActivity extends BaseActivity implements SearchAdapter.AdapterCallback {


    @BindView(R.id.rviHistory)
    RecyclerView rviHistory;
    @BindView(R.id.rviSearch)
    RecyclerView rviSearch;
    @BindView(R.id.rlaHistory)
    RelativeLayout rlaHistory;

    @BindView(R.id.eteSearch)
    EditText eteSearch;
    @BindView(R.id.butCancel)
    ImageButton butCancel;
    @BindView(R.id.tviEmptyData)
    TextView tviEmptyData;



    private RecyclerView.LayoutManager layoutManagerHistory;
    private RecyclerView.LayoutManager layoutManagerSearch;
    private SearchAdapter historyAdapter;
    private SearchAdapter searchAdapter;
    protected List<Definition> lstHistory = new ArrayList<>();
    protected List<Definition> lstDefinition = new ArrayList<>();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        ButterKnife.bind(this);
        setTitle(getString(R.string.title_busqueda));
        getSupportActionBar().setElevation(0);
        showHistory();
        showSearch();

        eteSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    if (eteSearch.getText().length() == 0) {
                        behaviorButCancel(false);
                        behaviorHistory(true);
                    } else
                        behaviorHistory(false);

                    hideKeyBoard();
                    eteSearch.setFocusableInTouchMode(false);
                    eteSearch.setFocusable(false);
                    eteSearch.setFocusableInTouchMode(true);
                    eteSearch.setFocusable(true);
                    return true;
                }
                return false;
            }
        });

        editTextlistener();

    }

    @Override
    protected void onResume() {
        super.onResume();


    }
    //region event

    @OnClick(R.id.butCancel)
    public void clickCancel() {
        if (eteSearch.getText().length() > 0) {
            //Code to request focus and show keyboard
            eteSearch.requestFocus();
            InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
            imm.showSoftInput(eteSearch, InputMethodManager.SHOW_IMPLICIT);

            eteSearch.setText("");
            updateHistory();
            behaviorButCancel(false);
        }
        behaviorHistory(true);
    }
    //endregion

    //region method

    public void showHistory() {
        generateListHistory();
        historyAdapter = new SearchAdapter(lstHistory, this, true);
        layoutManagerHistory = new LinearLayoutManager(this);

        rviHistory.setHasFixedSize(true);
        rviHistory.addItemDecoration(new SimpleDividerItemDecoration(this));
        rviHistory.setLayoutManager(layoutManagerHistory);
        rviHistory.setAdapter(historyAdapter);

    }
    public void updateHistory() {
        generateListHistory();
        historyAdapter.clear();
        historyAdapter.addAll(lstHistory);
        searchAdapter.setLenghtBold(Constants.TEXT_LENGHT_DEFAULT);
    }

    public void showSearch() {
        searchAdapter = new SearchAdapter(lstDefinition, this, false);
        layoutManagerSearch = new LinearLayoutManager(this);

        rviSearch.setHasFixedSize(true);
        rviSearch.addItemDecoration(new SimpleDividerItemDecoration(this));
        rviSearch.setLayoutManager(layoutManagerSearch);
        rviSearch.setAdapter(searchAdapter);

    }

    private void generateListHistory() {

        DefinitionDao definitionDao = getDaoSession().getDefinitionDao();
        QueryBuilder queryBuilder = definitionDao.queryBuilder();
        queryBuilder.where(DefinitionDao.Properties.IsHistory.eq(true));
        queryBuilder.orderDesc(DefinitionDao.Properties.CreationDate);
        lstHistory = queryBuilder.list();

    }

    private void editTextlistener() {

        eteSearch.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub
                //Left Trim
                String query = s.toString().replaceAll("^\\s+","");
                DefinitionDao definitionDao = getDaoSession().getDefinitionDao();

                QueryBuilder queryBuilder = definitionDao.queryBuilder();
                queryBuilder.where(DefinitionDao.Properties.WorldASCII.like(query + "%"));

                queryBuilder.orderAsc(DefinitionDao.Properties.WorldASCII);
                lstDefinition = queryBuilder.list();
                searchAdapter.clear();
                searchAdapter.addAll(lstDefinition);
                searchAdapter.setLenghtBold(query.length());

                validatePresentation();
                behaviorButCancel(true);
                behaviorHistory(false);


            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (TextUtils.isEmpty(s)) {
                    behaviorButCancel(false);
                    behaviorHistory(true);
                }
            }
        });
    }

    private void behaviorButCancel(boolean show) {
        if (show)
            butCancel.setVisibility(View.VISIBLE);
        else
            butCancel.setVisibility(View.GONE);
    }

    private void behaviorHistory(boolean showHistory) {
        if (showHistory) {
            rviSearch.setVisibility(View.GONE);
            rlaHistory.setVisibility(View.VISIBLE);
        } else {
            rviSearch.setVisibility(View.VISIBLE);
            rlaHistory.setVisibility(View.GONE);
        }
    }

    private void validatePresentation() {
        if (lstDefinition.isEmpty()) {
            tviEmptyData.setVisibility(View.VISIBLE);
            tviEmptyData.setText(getString(R.string.general_list_zero_data));
        } else {
            tviEmptyData.setVisibility(View.GONE);
        }

    }

    @Override
    public void onInsertHistoryCallback(Definition definition) {
        DefinitionDao definitionDao = getDaoSession().getDefinitionDao();
        definitionDao.insertOrReplace(definition);

    }
    //endregion


}
