package pe.com.ricc.atlas.adapter;

import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import pe.com.ricc.atlas.R;
import pe.com.ricc.atlas.activity.definition.DefinitionActivity;
import pe.com.ricc.atlas.model.Definition;
import pe.com.ricc.atlas.util.Constants;


/**
 * Created by Lenovo on 19/07/2016.
 */
public class RecentAdapter extends RecyclerView.Adapter<RecentAdapter.ViewHolder> {

    private List<Definition> lstDefinition;
    private AdapterCallback mAdapterCallback;

    public RecentAdapter(List<Definition> definitionList, AdapterCallback mAdapterCallback) {
        this.lstDefinition = definitionList;
        this.mAdapterCallback = mAdapterCallback;

    }


    @Override
    public RecentAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.item_recent, viewGroup, false);

        return new ViewHolder(v, mAdapterCallback);
    }

    @Override
    public void onBindViewHolder(RecentAdapter.ViewHolder viewHolder, int position) {
        Definition definition = lstDefinition.get(position);

        viewHolder.tviWorld.setText(definition.getWorld());
        viewHolder.definition = definition;

    }


    public void clear() {
        if (lstDefinition == null)
            lstDefinition = new ArrayList<>();

        lstDefinition.clear();
        notifyDataSetChanged();
    }

    // Add a list of items
    public void addAll(List<Definition> list) {
        lstDefinition = new ArrayList<>();
        lstDefinition.addAll(list);
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return lstDefinition.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        @BindView(R.id.tviWorld)
        TextView tviWorld;
        Definition definition;
        private AdapterCallback mAdapterCallback;

        public ViewHolder(View view, AdapterCallback mAdapterCallback) {
            super(view);
            this.mAdapterCallback = mAdapterCallback;
            ButterKnife.bind(this, view);


            view.setClickable(true);
            view.setOnClickListener(this);

        }

        @Override
        public void onClick(View v) {
            definition.setIsHistory(true);


            Intent intent = new Intent(v.getContext(), DefinitionActivity.class);
            intent.putExtra(Constants.INT_DEFINITION_OBJECT, definition);
            v.getContext().startActivity(intent);

        }

    }

    public interface AdapterCallback {
        void onInsertHistoryCallback(Definition definition);
    }
}
