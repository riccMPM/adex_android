package pe.com.ricc.atlas.model;

/**
 * Created by Lenovo on 10/11/2016.
 */

public class ActionMenu {
    private int idAction;
    private String action;

    public ActionMenu(int idAction, String action) {
        this.idAction = idAction;
        this.action = action;
    }

    public int getIdAction() {
        return idAction;
    }

    public void setIdAction(int idAction) {
        this.idAction = idAction;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }
}
