package pe.com.ricc.atlas.activity;


import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;


import pe.com.ricc.atlas.R;


/**
 * Created by Lenovo on 10/10/2016.
 */
public class LauncherActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_launcher);
        //Load BD for firstTime
        if (getIsSyncFirstTime()) {
            generateDefinitionManual();
            setIsSyncFirstTime(false);
        }
        generateRandomDefinition();
        goMainScreen();

    }



    @Override
    protected void onResume() {
        super.onResume();

    }
    public void goMainScreen() {

        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            public void run() {
                Intent launcherIntent;
                launcherIntent = new Intent(LauncherActivity.this, MainActivity.class);


                LauncherActivity.this.startActivity(launcherIntent);
                LauncherActivity.this.finish();
            }
        }, 1500);
    }


}