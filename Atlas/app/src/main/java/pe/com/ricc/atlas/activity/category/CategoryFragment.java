package pe.com.ricc.atlas.activity.category;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import com.simplecityapps.recyclerview_fastscroll.views.FastScrollRecyclerView;

import org.greenrobot.greendao.query.QueryBuilder;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import pe.com.ricc.atlas.R;
import pe.com.ricc.atlas.activity.BaseFragment;
import pe.com.ricc.atlas.activity.search.SearchActivity;
import pe.com.ricc.atlas.adapter.CategoryAdapter;
import pe.com.ricc.atlas.adapter.SearchAdapter;
import pe.com.ricc.atlas.dao.DefinitionDao;
import pe.com.ricc.atlas.model.Definition;
import pe.com.ricc.atlas.util.Constants;
import pe.com.ricc.atlas.util.EnumTipoCategoria;
import pe.com.ricc.atlas.util.FunctionsUtil;

/**
 * Created by Riccardomp on 1/05/17.
 */

public class CategoryFragment extends BaseFragment implements CategoryAdapter.AdapterCallback {

    @BindView(R.id.rviCategory)
    FastScrollRecyclerView rviCategory;
    @BindView(R.id.eteSearch)
    EditText eteSearch;
    @BindView(R.id.butCancel)
    ImageButton butCancel;
    @BindView(R.id.tviEmptyData)
    TextView tviEmptyData;


    private RecyclerView.LayoutManager layoutManager;
    private CategoryAdapter adapter;
    protected List<Definition> lstDefinition = new ArrayList<>();
    private int idCategory;


    public static CategoryFragment newInstance(int idCategory) {
        CategoryFragment fragment = new CategoryFragment();
        Bundle bundle = new Bundle();
        bundle.putInt(Constants.BUNDLE_NAVIGATION, idCategory);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_category, container, false);
        setHasOptionsMenu(true);
        ButterKnife.bind(this, view);
        idCategory = getArguments().getInt(Constants.BUNDLE_NAVIGATION);

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        setTitle(getTitle());
        eteSearch.setHint(String.format(getString(R.string.category_serach_in), getTitle()));
        showCategory();

        eteSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    if (eteSearch.getText().length() == 0)
                        behaviorButCancel(false);


                    hideKeyBoard();
                    eteSearch.setFocusableInTouchMode(false);
                    eteSearch.setFocusable(false);
                    eteSearch.setFocusableInTouchMode(true);
                    eteSearch.setFocusable(true);
                    return true;
                }
                return false;
            }
        });

        editTextlistener();

    }

    @Override
    public void onResume() {
        super.onResume();

    }

    //region event
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.general_menu, menu);
        MenuItem item = menu.findItem(R.id.action_clear);
        item.setVisible(false);//
        item = menu.findItem(R.id.action_share);
        item.setVisible(false);//
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_search:
                clickSearch();
                break;
        }
        return true;
    }

    @Override
    public void onInsertHistoryCallback(Definition definition) {
        DefinitionDao definitionDao = getDaoSession().getDefinitionDao();
        definitionDao.insertOrReplace(definition);

    }

    @OnClick(R.id.butCancel)
    public void clickCancel() {
        if (eteSearch.getText().length() > 0) {
            //Code to request focus and show keyboard
            eteSearch.requestFocus();
            InputMethodManager imm = (InputMethodManager) getContext().getSystemService(getContext().INPUT_METHOD_SERVICE);
            imm.showSoftInput(eteSearch, InputMethodManager.SHOW_IMPLICIT);

            eteSearch.setText("");
            updateCategory();
            behaviorButCancel(false);
        }

    }
    //endregion

    //region method


    public void clickSearch() {
        Intent intent = new Intent(getContext(), SearchActivity.class);
        startActivity(intent);
    }

    public void showCategory() {
        generateListCategory();
        adapter = new CategoryAdapter(lstDefinition, idCategory, this, getContext());
        layoutManager = new LinearLayoutManager(getContext());

        rviCategory.setHasFixedSize(true);
        rviCategory.setLayoutManager(layoutManager);
        rviCategory.setAdapter(adapter);

    }


    private void generateListCategory() {
        DefinitionDao definitionDao = getDaoSession().getDefinitionDao();
        QueryBuilder queryBuilder = definitionDao.queryBuilder();
        queryBuilder.orderAsc(DefinitionDao.Properties.WorldASCII);

        //Get condition by Category
        FunctionsUtil.getCategoryConditionWhere(queryBuilder,idCategory);

        lstDefinition = queryBuilder.list();

        //We generate to show the capittal letter for the category
        for (char alphabet = 'a'; alphabet <= 'z'; alphabet++) {
            for (Definition definition : lstDefinition) {
                String firstLetter = FunctionsUtil.getFirstLetterWorld(definition.getWorld());

                if (firstLetter.toLowerCase().equals(String.valueOf(alphabet).toLowerCase())) {
                    definition.setIsShowLetter(true);
                    break;
                }
            }
        }
    }

    private String getTitle() {
        EnumTipoCategoria enumTipoInformacion = EnumTipoCategoria.values()[idCategory];
        return  enumTipoInformacion.getDescripcion();

    }

    public void updateCategory() {
        generateListCategory();
        adapter.clear();
        adapter.addAll(lstDefinition);
        adapter.setLenghtBold(Constants.TEXT_LENGHT_DEFAULT);
    }

    private void behaviorButCancel(boolean show) {
        if (show)
            butCancel.setVisibility(View.VISIBLE);
        else
            butCancel.setVisibility(View.GONE);
    }

    private void validatePresentation() {
        if (lstDefinition.isEmpty()) {
            tviEmptyData.setVisibility(View.VISIBLE);
            tviEmptyData.setText(getString(R.string.general_list_zero_data));
        } else {
            tviEmptyData.setVisibility(View.GONE);
        }

    }

    private void editTextlistener() {

        eteSearch.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub
                //Left Trim
                String query = s.toString().replaceAll("^\\s+","");
                DefinitionDao definitionDao = getDaoSession().getDefinitionDao();

                QueryBuilder queryBuilder = definitionDao.queryBuilder();
                queryBuilder.where(DefinitionDao.Properties.WorldASCII.like(query + "%"));
                //Get condition by Category
                FunctionsUtil.getCategoryConditionWhere(queryBuilder,idCategory);

                queryBuilder.orderAsc(DefinitionDao.Properties.WorldASCII);
                lstDefinition = queryBuilder.list();
                adapter.clear();
                adapter.addAll(lstDefinition);
                adapter.setLenghtBold(query.length());

                validatePresentation();
                behaviorButCancel(true);


            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (TextUtils.isEmpty(s))
                    behaviorButCancel(false);

            }
        });
    }
    //endregion


}