package pe.com.ricc.atlas.activity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;


import com.google.gson.Gson;

import org.greenrobot.greendao.query.Query;

import pe.com.ricc.atlas.application.AtlasApplication;
import pe.com.ricc.atlas.dao.DaoSession;
import pe.com.ricc.atlas.dao.DefinitionDao;
import pe.com.ricc.atlas.model.Definition;
import pe.com.ricc.atlas.util.Constants;
import pe.com.ricc.atlas.util.FunctionsUtil;

/**
 * Created by Lenovo on 12/10/2016.
 */
public class BaseFragment extends Fragment {

    private SharedPreferences appPreferences;
    private DaoSession daoSession;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        daoSession = ((AtlasApplication) getContext().getApplicationContext()).getDaoSession();
        appPreferences = getActivity().getSharedPreferences(Constants.PREFERENCES_NAME, getActivity().MODE_PRIVATE);

    }


    //region method
    public DaoSession getDaoSession() {
        return daoSession;
    }

    public SharedPreferences getAppPreferences() {
        return appPreferences;
    }


    public void hideKeyBoard() {
        View view = getActivity().getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    public void setTitle(String title) {
        getActivity().setTitle(title);
    }

    public void showAlert(String msg) {
        AlertDialog alertDialog = new AlertDialog.Builder(getActivity()).create();
        alertDialog.setMessage(msg);
        alertDialog.setButton(DialogInterface.BUTTON_NEGATIVE, "OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        alertDialog.show();
    }


    public void showToast(String message) {
        Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
    }


    public void generateRandomDefinition() {

        boolean isGetNewWorldDay = (getDateWordDay().equals(FunctionsUtil.getCurrentDate()) ?
                false : true);
        if (isGetNewWorldDay) {
            DefinitionDao definitionDao = getDaoSession().getDefinitionDao();
            Query<Definition> query = definitionDao.queryRawCreate(
                    "ORDER BY RANDOM() LIMIT 1");

            setDateWorldDay(FunctionsUtil.getCurrentDate());

            try {
                setDefinitionDay(query.unique());
            } catch (Exception e) {
                Log.i(Constants.LOG_APP_TAG, e.getMessage());
            }

        }

    }


    //endregion

    //region preference
    public void setDefinitionDay(Definition definition) {
        appPreferences.edit().putString(Constants.PREF_DEFINITION_DAY, new Gson().toJson(definition)).apply();
    }

    public Definition getDefinitionDay() {
        if (appPreferences.contains(Constants.PREF_DEFINITION_DAY))
            return new Gson().fromJson(appPreferences.getString(Constants.PREF_DEFINITION_DAY, ""), Definition.class);
        else
            return null;

    }

    /*
    * Set the date world of the day
    * */
    public void setDateWorldDay(String date) {
        appPreferences.edit().putString(Constants.PREF_DATE_WORD_DAY, date).apply();
    }

    public String getDateWordDay() {
        if (appPreferences.contains(Constants.PREF_DATE_WORD_DAY))
            return appPreferences.getString(Constants.PREF_DATE_WORD_DAY, "");
        else
            return "";
    }

    //endregion
}
