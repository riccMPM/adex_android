package pe.com.ricc.atlas.model;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.NotNull;

import java.io.Serializable;

// THIS CODE IS GENERATED BY greenDAO, EDIT ONLY INSIDE THE "KEEP"-SECTIONS

// KEEP INCLUDES - put your custom includes here
// KEEP INCLUDES END

/**
 * Entity mapped to table "DEFINITION".
 */
@Entity
public class Definition implements Serializable {

    private static final long serialVersionUID = 87996564786747166L;

    @Id
    private String id;
    private Integer idFather;

    @NotNull
    private String world;

    @NotNull
    private String worldASCII;
    private String definition;
    private Boolean isTerrestral;
    private Boolean isMaritime;
    private Boolean isAereal;
    private Boolean isINCOTERMS;
    private Boolean isOperadorComercioExterior;
    private Boolean isAcuerdoComercialPeru;
    private Boolean isTipoContenedores;
    private Boolean isTipoCarga;
    private Boolean isTipoBuqueMercante;
    private Boolean isREGIMENS;
    private String urlImage;
    private String urlVideo;
    private Boolean isHistory;
    private Boolean isShowLetter;
    private java.util.Date creationDate;
    private String source;

    // KEEP FIELDS - put your custom fields here
    // KEEP FIELDS END

    @Generated(hash = 76402987)
    public Definition() {
    }

    public Definition(String id) {
        this.id = id;
    }

    @Generated(hash = 1306764460)
    public Definition(String id, Integer idFather, @NotNull String world, @NotNull String worldASCII, String definition, Boolean isTerrestral, Boolean isMaritime, Boolean isAereal, Boolean isINCOTERMS, Boolean isOperadorComercioExterior, Boolean isAcuerdoComercialPeru, Boolean isTipoContenedores, Boolean isTipoCarga, Boolean isTipoBuqueMercante, Boolean isREGIMENS, String urlImage, String urlVideo, Boolean isHistory, Boolean isShowLetter, java.util.Date creationDate,
            String source) {
        this.id = id;
        this.idFather = idFather;
        this.world = world;
        this.worldASCII = worldASCII;
        this.definition = definition;
        this.isTerrestral = isTerrestral;
        this.isMaritime = isMaritime;
        this.isAereal = isAereal;
        this.isINCOTERMS = isINCOTERMS;
        this.isOperadorComercioExterior = isOperadorComercioExterior;
        this.isAcuerdoComercialPeru = isAcuerdoComercialPeru;
        this.isTipoContenedores = isTipoContenedores;
        this.isTipoCarga = isTipoCarga;
        this.isTipoBuqueMercante = isTipoBuqueMercante;
        this.isREGIMENS = isREGIMENS;
        this.urlImage = urlImage;
        this.urlVideo = urlVideo;
        this.isHistory = isHistory;
        this.isShowLetter = isShowLetter;
        this.creationDate = creationDate;
        this.source = source;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Integer getIdFather() {
        return idFather;
    }

    public void setIdFather(Integer idFather) {
        this.idFather = idFather;
    }

    @NotNull
    public String getWorld() {
        return world;
    }

    /** Not-null value; ensure this value is available before it is saved to the database. */
    public void setWorld(@NotNull String world) {
        this.world = world;
    }

    @NotNull
    public String getWorldASCII() {
        return worldASCII;
    }

    /** Not-null value; ensure this value is available before it is saved to the database. */
    public void setWorldASCII(@NotNull String worldASCII) {
        this.worldASCII = worldASCII;
    }

    public String getDefinition() {
        return definition;
    }

    public void setDefinition(String definition) {
        this.definition = definition;
    }

    public Boolean getIsTerrestral() {
        return isTerrestral;
    }

    public void setIsTerrestral(Boolean isTerrestral) {
        this.isTerrestral = isTerrestral;
    }

    public Boolean getIsMaritime() {
        return isMaritime;
    }

    public void setIsMaritime(Boolean isMaritime) {
        this.isMaritime = isMaritime;
    }

    public Boolean getIsAereal() {
        return isAereal;
    }

    public void setIsAereal(Boolean isAereal) {
        this.isAereal = isAereal;
    }

    public Boolean getIsINCOTERMS() {
        return isINCOTERMS;
    }

    public void setIsINCOTERMS(Boolean isINCOTERMS) {
        this.isINCOTERMS = isINCOTERMS;
    }

    public Boolean getIsOperadorComercioExterior() {
        return isOperadorComercioExterior;
    }

    public void setIsOperadorComercioExterior(Boolean isOperadorComercioExterior) {
        this.isOperadorComercioExterior = isOperadorComercioExterior;
    }

    public Boolean getIsAcuerdoComercialPeru() {
        return isAcuerdoComercialPeru;
    }

    public void setIsAcuerdoComercialPeru(Boolean isAcuerdoComercialPeru) {
        this.isAcuerdoComercialPeru = isAcuerdoComercialPeru;
    }

    public Boolean getIsTipoContenedores() {
        return isTipoContenedores;
    }

    public void setIsTipoContenedores(Boolean isTipoContenedores) {
        this.isTipoContenedores = isTipoContenedores;
    }

    public Boolean getIsTipoCarga() {
        return isTipoCarga;
    }

    public void setIsTipoCarga(Boolean isTipoCarga) {
        this.isTipoCarga = isTipoCarga;
    }

    public Boolean getIsTipoBuqueMercante() {
        return isTipoBuqueMercante;
    }

    public void setIsTipoBuqueMercante(Boolean isTipoBuqueMercante) {
        this.isTipoBuqueMercante = isTipoBuqueMercante;
    }

    public Boolean getIsREGIMENS() {
        return isREGIMENS;
    }

    public void setIsREGIMENS(Boolean isREGIMENS) {
        this.isREGIMENS = isREGIMENS;
    }

    public String getUrlImage() {
        return urlImage;
    }

    public void setUrlImage(String urlImage) {
        this.urlImage = urlImage;
    }

    public String getUrlVideo() {
        return urlVideo;
    }

    public void setUrlVideo(String urlVideo) {
        this.urlVideo = urlVideo;
    }

    public Boolean getIsHistory() {
        return isHistory;
    }

    public void setIsHistory(Boolean isHistory) {
        this.isHistory = isHistory;
    }

    public Boolean getIsShowLetter() {
        return isShowLetter;
    }

    public void setIsShowLetter(Boolean isShowLetter) {
        this.isShowLetter = isShowLetter;
    }

    public java.util.Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(java.util.Date creationDate) {
        this.creationDate = creationDate;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    // KEEP METHODS - put your custom methods here
    // KEEP METHODS END

}